@extends('layouts.master')
@section('titulo')
Mostrar
@endsection
@section('contenido')
<h1>{{ $modalidad['nombre'] }}</h1>
<p>Familia profesional<h6>{{ $modalidad['familiaProfesional'] }}</h6></p>
<p>Participantes</p>
@foreach( $participantes as $participante)
		<div class="col-xs-12col-sm-6col-md-4">
			<h6>{{ $participante['nombre'] }}</h6>
			<img src="{{asset('assets/imagenes/participantes')}}/{{ $participante['imagen']}}" style="height:200px"/>
		</div>
	@endforeach
	<a href="/laravel_skills_Anibal/public/modalidades/puntuar/slug">Puntuar</a>
	<a href="/laravel_skills_Anibal/public/modalidades/resetear/slug">Resetear</a>
@endsection