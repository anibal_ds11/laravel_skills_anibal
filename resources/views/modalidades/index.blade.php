@extends('layouts.master')
@section('titulo')
Indice
@endsection
@section('contenido')
Pantalla principal
<div class="row">
	@foreach( $arrayModalidades as $clave=> $modalidad)
		<div class="col-xs-12col-sm-6col-md-4">
			<a href="{{ url('/modalidades/mostrar/' . $modalidad['slug']) }}">				
			<h6>{{ $modalidad['nombre'] }}</h6>
			<img src="{{asset('assets/imagenes/modalidades')}}/{{ $modalidad['imagen']}}" style="height:200px"/>
			<p>{{$modalidad->getParticipantes()->count()}}</p>
			</a>
		</div>
	@endforeach
</div>
@endsection