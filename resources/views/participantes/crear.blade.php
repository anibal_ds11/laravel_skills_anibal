 @extends("layouts.master")

@section("titulo")
 Veterinario
@endsection
@section("contenido") 
<div class="row">
 <div class="offset-md-3 col-md-6">
 <div class="card">
 <div class="card-header text-center">
 Añadir nuevo cuadro
 </div>
 <div class="card-body" style="padding:30px">
 {{-- TODO: Abrir el formulario e indicar el método POST --}}
 <form action ="{{action('ParticipantesController@postCrear')}}" method="POST" enctype="multipart/form-data">
  {{ csrf_field()}}
 <div class="form-group">
 <label for="nombre">Nombre</label>
 <input type="text" name="nombre" id="nombre" class="form-control">
 </div>
 <div class="form-group">
 <label for="nombre">Apellidos</label>
 <input type="text" name="apellidos" id="apellidos" class="form-control">
 </div>
 <div class="form-group">
 <label for="nombre">Centro</label>
 <input type="text" name="centro" id="centro" class="form-control">
 </div>
 <div class="form-group">
 <label for="nombre">Tutor</label>
 <input type="text" name="tutor" id="tutor" class="form-control">
 </div>
 <div class="form-group">
 <label for="nombre">Fecha de nacimiento</label>
 <input type="date" name="fechaNacimiento" id="Nacimiento" class="form-control">
 </div>
  <div class="form-group">
  	Modalidad<select>
  		@foreach($arrayModalidades as $modalidad)
  		<option name="{{$modalidad->nombre}}">{{$modalidad->nombre}}</option>
  		@endforeach
  	</select>

  </div>
 <div class="form-group">
  <label for="imagen">Imagen</label>
 <input type="file" id="imagen"  name="imagen" class="form-control"></input>
 </div>
 <div class="form-group text-center">
 <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">
 Añadir participante
 </button>
 </div>
  </form>
 </div>
 </div>
 </div>
</div>
@endsection