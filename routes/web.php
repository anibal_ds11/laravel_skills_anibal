<?php
use App\Modalidad;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ModalidadesController@getInicio');

Route::get('modalidades','ModalidadesController@getTodas'); 

Route::get('modalidades/mostrar/{slug}','ModalidadesController@getMostrar');

Route::get('modalidades/puntuar/{slug}','ModalidadesController@getPuntuacion'); 

Route::get('modalidades/resetear/{slug}','ModalidadesController@getReset'); 

Route::get('participantes/inscribir','ParticipantesController@getCrear'); 

Route::post('participantes/inscribir','ParticipantesController@postCrear');

Route::get('rest/ganador/{slug}','RestWebServiceController@getGanador');
