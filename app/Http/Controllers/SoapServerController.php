<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use SoapServer;
use App\Participante;
use App\Modalidad;
use App\AnibalLib\WSDLDocument;

class SoapServerController extends Controller
{	
    private $clase="\\App\\Http\\Controllers\\SkillsWebService";
    private $uri="http://127.0.0.1/laravel_skills_Anibal/public/participantes/inscribir/api";
    private $urlWSDL="http://127.0.0.1/laravel_skills_Anibal/public/participantes/inscribir/api/wsdl";

    public function getServer(){
    	$server = new SoapServer($this->urlWSDL);
    	$server->setClass($this->clase);
    	$server->handle();
    	exit;
    }
    public function getWSDL()
    {
    	$wsdl= new WSDLDocument($this->clase,$this->uri,$this->uri)	;
    	$wsdl->formatOutput=true;
		header('Content-Type: text/xml');
		echo $wsdl->saveXML();
		exit;
    }
}
class SkillsWebService
{	/**
	 * Numerod participantes
	 * @param string $centro 
	 * @return int
	 */
	public function getNumeroParticipantes($centro){
		return Participante::where('centro',$centro)->count();
	}

	/**
	 * Devuelve los id de los participantes segun un tutor
	 * @param string $tutor 
	 * @return int[] 
	 */
	public function getParticipantesTutor($tutor)
	{
		return Participante::where('tutor',$tutor)->get()->orderBy('puntos','desc');
	}

}
