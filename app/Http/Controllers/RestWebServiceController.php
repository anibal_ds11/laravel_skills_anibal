<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;
use Illuminate\Support\Str;

class RestWebServiceController extends Controller
{
    public function getGanador($slug)
    {
    	$modalidad = Modalidad::where('slug',$slug)->first();        
        $participante=Participante::where('modalidad_id',$modalidad->id)->orderBy('puntos','desc')->first();
    	return response()->json($participante);
    }
    
  
}
