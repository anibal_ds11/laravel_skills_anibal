<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;

class ModalidadesController extends Controller
{
    public function getInicio()
    {
     return redirect()->action('ModalidadesController@getTodas');
    }
    public function getTodas()
    {
    	return view("modalidades.index",array('arrayModalidades'=>Modalidad::all()));
    }
    public function getMostrar($slug)
    {
    	$modalidad = Modalidad::where('slug',$slug)->first();
        $participantes = Participante::where('modalidad_id',$modalidad->id)->orderBy('puntos')->get();
        return view('modalidades.mostrar',array('modalidad'=>$modalidad,'participantes'=>$participantes));
    }
    public function getPuntuacion()
    {
    	$participantes = Participante::all();
    	foreach ($participantes as $participante) {
    		$participante->puntos = Math.rand(100);
    		$participante->save();
    	}
    	return redirect('modalidades')->with('mensaje','Done!');
    }
    public function getReset()
    {
    	$participantes = Participante::all();
    	foreach ($participantes as $participante) {
    		$participante->puntos = -1;
    		$participante->save();
    	}
    	return redirect('modalidades')->with('mensaje','Done!');
    }
}
