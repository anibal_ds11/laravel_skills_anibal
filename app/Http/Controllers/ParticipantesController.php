<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modalidad;
use App\Participante;
class ParticipantesController extends Controller
{
    public function getCrear()
    {
    	return view('participantes.crear',array('arrayModalidades'=>Modalidad::all()));
    }
    public function postCrear(Request $request)
    {
    	$participante = new Participante();
    	$participante->nombre = $request->nombre;
    	$participante->apellidos = $request->apellidos;
    	$participante->centro = $request->centro;
    	$participante->tutor = $request->tutor;
    	$participante->fechaNacimiento= $request->fechaNacimiento;
    	$participante->modalidad_id=Modalidad::where('nombre',$request->nombre)->firstOrFail();
    	$mascota->imagen = $request->imagen->store('','mascotas');
    	try{
    		$participante->save();
    		return redirect('modalidades')->with('mensaje','Participante insertado');
    	}catch(\Illuminate\Database\QueryException $ex){
    		return redirect('modalidades')->with('mensaje','Fallo al crear el participante');
    	}
    }
}
