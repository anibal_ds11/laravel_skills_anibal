<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Modalidad extends Model
{
	protected $table='modalidades';

	 public function getParticipantes()
    {
    	return $this->hasMany('App\Participante','modalidad_id','id');
    }
}

